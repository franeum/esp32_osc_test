#include <Arduino.h>
#include <WiFi.h>
#include <ArduinoOSCWiFi.h>

const char *ssid = "nome della tua rete";
const char *pwd = "password della tua rete";
// indirizzo ip della tua rete, lo ottiene nella powershell con il comando ipconfig
const char *host = "192.168.1.132";

// porta di invio dei messaggi (la userai in max)
const int send_port = 55555;

OscWiFiClient client;

void setup() {
    Serial.begin(115200);
    delay(2000);

    // WiFi stuff (no timeout setting for WiFi)
#if defined(ESP_PLATFORM) || defined(ARDUINO_ARCH_RP2040)
#ifdef ESP_PLATFORM
    WiFi.disconnect(true, true);  // disable wifi, erase ap info
#else
    WiFi.disconnect(true);  // disable wifi
#endif
    delay(1000);
    WiFi.mode(WIFI_STA);
#endif

    WiFi.begin(ssid, pwd);


    while (WiFi.status() != WL_CONNECTED) {
        Serial.print(".");
        delay(500);
    }
    Serial.print("WiFi connected, IP = ");
    Serial.println(WiFi.localIP());
}

void loop() {
    int value = random(100);
    client.send(host, send_port, "/testmessage", value);
    delay(1000);
}
